# Migrate from Remember the Milk to Taskwarrior

## How to use

### Setup

1. Install [Taskwarrior](https://taskwarrior.org/). Running `task` should invoke it.
1. Set up two extra user-defined attributes: `URL` for a Remember the Milk task "URL" and `RTMid` for the source task id.
    1. `task config uda.url.label URL`
    1. `task config uda.url.type string`
    1. `task config uda.rtmid.label RTMid`
    1. `task config uda.rtmid.type string`
1. Save a Remember the Milk JSON export at `rtm.json`.
1. Save the output of `task export` at `tw.json`.

### Import

    python3 import-rtm-json.py rtm.json tw.json | task import

## Design notes

* RTM "list" is lowercased and used as a tag in TW. Anything in the RTM "Work" list will get the tag "work".
* Do not import recurring tasks.

## TODO

* fix "completed" date handling
    * I think I should use [end](https://taskwarrior.org/docs/terminology.html#end) instead of `date_modified`
* handle recurrence
    * I didn't need this right away because I only have a few recurring tasks - easier to just ignore them
* handle notes
    * convert to TW annotations
* auto set up URL user-defined attribute
    * RTM comes with this and TW does not, so two config commands or direct changes to ~/.taskrc are necessary.
* sanity check command-line arguments
    * first argument must be a readable file, is JSON, looks like RTM JSON, etc
    * second argument must be a readable file, is JSON, looks like TW JSON, etc

# Copyleft and License

* Copyright ©2018 Adam Monsen &lt;haircut@gmail.com&gt;
* License: AGPL v3 or later (see COPYING)
