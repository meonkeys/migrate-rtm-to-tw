#!/usr/bin/python3

# import-rtm-json.py - Import Remember the Milk JSON export into Taskwarrior
# Copyright (C) 2018 Adam Monsen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os
import sys

# map RTM's P1/P2/P3/PN to H, M, L, None/nil in TW
priorityMap = {
    'P1': 'H',
    'P2': 'M',
    'P3': 'L',
}
# TODO date_due means wait until AND make it due... handle this

def getListsMap(lists):
    '''build map of lists IDs to list data for later lookup - RTM field with
    string name like "Personal" is `list_id`'''
    listsMap = {}
    for l in lists:
        listsMap[l['id']] = l['name']
    return listsMap

# fetch and loop over tasks, importing into TW if not already imported
with open(sys.argv[1]) as jsonRtmDump:
    rtmJson = json.load(jsonRtmDump)

listsMap = getListsMap(rtmJson['lists'])

with open(sys.argv[2]) as jsonTwDump:
    twJson = json.load(jsonTwDump)

twRtmidToTwTask = {}
for twTask in twJson:
    if 'rtmid' in twTask:
        twRtmidToTwTask[twTask['rtmid']] = twTask

def lookupTwTaskByRtmId(rtmId):
    if rtmId in twRtmidToTwTask:
        return twRtmidToTwTask[rtmId]
    return None

def rtmDateToTwDate(rtmDate):
    return str(rtmDate)[:-3]

importable = []

# only allow one task with a given name
duplicatePreventer = {}

# map of RTM attributes to TW attributes (maybe move to README)
#   id             → id
#   list_id        → list name is used as a tag
#   date_added     → entry
#   date_created   → entry (duplicate?)
#   date_due       → due
#   date_modified  → modified
#   priority       → priority
#   date_completed → combination of status=completed and date_modified
#   date_trashed   → deleted, I assume
#   url            → URL (a user-defined attribute)
#   ...recurrence?

def prepForImport(rtmTask, importable):
    # only allow one task with a given name
    if rtmTask['name'] in duplicatePreventer:
        return None
    duplicatePreventer[rtmTask['name']] = 1
    # ignore repeating tasks
    if 'repeat' in rtmTask:
        return None
    # ignore deleted tasks
    if 'date_trashed' in rtmTask:
        return None
    tags = rtmTask['tags']
    tags.append(listsMap[rtmTask['list_id']].lower())
    newTwTask = {
        'rtmid': rtmTask['id'],
        'description': rtmTask['name'],
        'entry': rtmDateToTwDate(rtmTask['date_created']),
        'modified': rtmDateToTwDate(rtmTask['date_modified']),
        'tags': tags,
    }
    if rtmTask['priority'] in priorityMap:
        newTwTask['priority'] = priorityMap[rtmTask['priority']]
    if 'url' in rtmTask:
        newTwTask['url'] = rtmTask['url']
    if 'date_completed' in rtmTask:
        newTwTask['status'] = 'completed'
    if 'date_due' in rtmTask:
        newTwTask['due'] = rtmDateToTwDate(rtmTask['date_due'])
        newTwTask['wait'] = rtmDateToTwDate(rtmTask['date_due'])
    importable.append(newTwTask)

for rtmTask in rtmJson['tasks']:
    rtmId = rtmTask['id']

    # check if task is already in TW; look for matching rtm_id user-defined
    # attribute via CLI
    if lookupTwTaskByRtmId(rtmId):
        pass # task has already been imported - skip
    else:
        prepForImport(rtmTask, importable)

print(json.dumps(importable))
